# Preparing

Run these commands into host before start containers

```
sysctl -w vm.max_map_count=524288
sysctl -w fs.file-max=131072
ulimit -n 131072
ulimit -u 8192
```

# Starting

```
docker-compose up -d
docker-compose logs -f
```

# First User for Gitlab

```
docker-compose exec gitlab bash
# gitlab-rake "gitlab:password:reset"
Username: root
Password: [your-password-here]
```

# URLS

* Sonar: http://localhost:9000
* Gitlab: http://localhost